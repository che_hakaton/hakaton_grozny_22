export const ApplicationConfig = {
  widget: undefined,
  AmoConstant: undefined,
  disposableToken: {
    token: undefined,
    expiring: undefined
  }
}

export const AmoConstant = () => {
  return ApplicationConfig.AmoConstant
}

export const setUpWidget = (widget) => {
  ApplicationConfig.widget = widget
}
export const setUpAmoConstant = (constant) => {
  ApplicationConfig.AmoConstant = constant
}

export const setUpToken = ({ token }) => {
  ApplicationConfig.disposableToken.token = token
  ApplicationConfig.disposableToken.expiring = Date.now() + 15e5
  const n = ApplicationConfig.disposableToken.expiring - Date.now()
  setTimeout(() => {
    ApplicationConfig.disposableToken.token = undefined
  }, n)
}
