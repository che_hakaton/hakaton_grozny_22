import axios from 'axios'
import { ApplicationConfig, setUpToken } from '@/constants'

const internal = axios.create({
  baseURL: process.env.VUE_APP_BASE_URL,
  cancelToken: axios.cancelToken
})

internal.interceptors.request.use(
  async (config) => {
    if (
      ApplicationConfig.disposableToken.expiring === undefined ||
      Date.now() > ApplicationConfig.disposableToken.expiring
    ) {
      const code = ApplicationConfig.widget.get_settings().oauth_client_uuid
      const response = await axios.get(
        `/ajax/v2/integrations/${code}/disposable_token`,
        {
          headers: {
            Accept: '*/*',
            'X-Requested-With': 'XMLHttpRequest'
          }
        }
      )
      if (response.status === 200) {
        setUpToken(response.data)
      }
    }
    config.headers['x-auth-token'] = ApplicationConfig.disposableToken.token
    return config
  },
  (error) => {
    return Promise.reject(error)
  }
)

export default internal
