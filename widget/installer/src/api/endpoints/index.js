
export const users = {
  get: {
    all: 'api/users/get',
    allWithGroups: 'api/users/get-with-groups',
    workingDays: 'api/users/get-working-days',
    updateUsersAndGroups: 'api/users/update-users-and-groups'
  },
  post: {
    delUserSchedule: 'api/users/delete-schedule',
    delUserDays: 'api/users/delete-days',
    delUserHolidays: 'api/users/delete-holidays'
  }
}

export const commonUtils = {
  get: {
    daysOfWeek: 'api/utils/days-of-week'
  }
}
