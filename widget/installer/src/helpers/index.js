import moment from 'moment'
export const getRandomColor = () => {
  const colors = [
    '#000000',
    '#0000ff',
    '#a52a2a',
    '#00008b',
    '#008b8b',
    '#006400',
    '#bdb76b',
    '#8b008b',
    '#556b2f',
    '#ff8c00',
    '#9932cc',
    '#8b0000',
    '#e9967a',
    '#9400d3',
    '#ff00ff',
    '#008000',
    '#4b0082',
    '#f0e68c',
    '#ffb6c1',
    '#00ff00',
    '#ff00ff',
    '#800000',
    '#000080',
    '#808000',
    '#ffa500',
    '#ffc0cb',
    '#800080',
    '#800080',
    '#ff0000'
  ]
  return colors[Math.floor(Math.random() * colors.length)]
}

export const enumerateDaysBetweenDates = (startDate, endDate, filter = undefined) => {
  const dates = []

  const currDate = moment(startDate).startOf('day')
  const lastDate = moment(endDate).startOf('day')
  do {
    if (filter === undefined || filter(currDate)) {
      dates.push(currDate.clone())
    }
  } while (currDate.add(1, 'days').diff(lastDate) <= 0)

  return dates
}
