import Vue from 'vue'
import Vuex from 'vuex'
import common from '@/store/modules/common'
import amoUsers from '@/store/modules/amoUsers'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    common,
    amoUsers
  }
})
