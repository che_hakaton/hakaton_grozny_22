import internal from '@/api/internal'
import { schedules, users } from '@/api/endpoints'

export default {
  actions: {
    async fetchUsersAndGroupsByAccount (ctx) {
      ctx.commit('updateLoading', { name: 'usersAndGroups', flag: true })
      try {
        const response = await internal.get(users.get.allWithGroups)
        if (response.status === 200) {
          ctx.commit('updateUsersAndGroups', response.data.payload)
          return true
        }
      } finally {
        ctx.commit('updateLoading', { name: 'usersAndGroups', flag: false })
      }
    },
    async fetchUsers (ctx) {
      ctx.commit('updateLoading', { name: 'users', flag: true })
      const response = await internal.get(users.get.all)
      if (response.status === 200) {
        ctx.commit('updateUsers', response.data.payload)
      }
      ctx.commit('updateLoading', { name: 'users', flag: false })
    },
    async saveUsersDaily (ctx, user) {
      ctx.commit('updateLoading', { name: 'users', flag: true })
      const response = await internal.post(schedules.post.saveDailySchedules, {
        user: user
      })
      if (response.status === 200) {
        ctx.commit('updateUsers', response.data.payload)
      }
      ctx.commit('updateLoading', { name: 'users', flag: false })
    },
    async deleteUserSchedule (ctx, user) {
      ctx.commit('updateLoading', { name: 'workingDays', flag: true })
      const { data } = await internal.post(users.post.delUserSchedule, {
        user: user
      })
      if (data.success) {
        ctx.commit('updateDays', { userId: user.user_id, days: data.payload.days, schedule: data.payload.schedule })
      }
      ctx.commit('updateLoading', { name: 'workingDays', flag: false })
    },
    async deleteUserDays (ctx, user) {
      ctx.commit('updateLoading', { name: 'workingDays', flag: true })
      const { data } = await internal.post(users.post.delUserDays, {
        user: user
      })
      if (data.success) {
        ctx.commit('updateDays', { userId: user.user_id, days: data.payload.days, schedule: data.payload.schedule })
      }
      ctx.commit('updateLoading', { name: 'workingDays', flag: false })
    },
    async deleteUserHolidays (ctx, user) {
      ctx.commit('updateLoading', { name: 'workingDays', flag: true })
      const { data } = await internal.post(users.post.delUserHolidays, {
        user: user
      })
      if (data.success) {
        ctx.commit('updateDays', { userId: user.user_id, days: data.payload.days, schedule: data.payload.schedule })
      }
      ctx.commit('updateLoading', { name: 'workingDays', flag: false })
    },
    async fetchUsersSchedule (ctx, user) {
      ctx.commit('updateLoading', { name: 'workingDays', flag: true })
      const { data } = await internal.get(users.get.workingDays, {
        params: {
          user: user
        }
      })
      if (data.success) {
        ctx.commit('updateDays', { userId: user, days: data.payload.days, schedule: data.payload.schedule })
      }
      ctx.commit('updateLoading', { name: 'workingDays', flag: false })
    },
    updateUsersAndGroups () {
      internal.get(users.get.updateUsersAndGroups)
    }
  },
  mutations: {
    updateUsersAndGroups (state, usersAndGroups) {
      usersAndGroups.map(x => {
        x.users.map(u => {
          u.days = []
          u.popover = false
          u.is_deleting = false
          return u
        })
        return x
      })
      state.usersAndGroups = usersAndGroups
    },
    updateUsers (state, usersAndGroups) {
      state.users = usersAndGroups
    },
    updateDays (state, { userId, days, schedule }) {
      state.usersAndGroups.map(ug => {
        const user = ug.users.find(u => u.user_id === userId)
        if (user !== undefined) {
          user.days = days
          user.schedule = schedule
          return ug
        }
      })
    }
  },
  state: {
    usersAndGroups: [],
    users: []
  },
  getters: {
    allUsersAndGroups (state) {
      return state.usersAndGroups
    },
    allUsers (state) {
      return state.users
    }
  }
}
