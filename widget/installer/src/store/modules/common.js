export default {
  state: {
    loading: {
      templateTypes: false,
      daysOfWeek: false,
      templates: false,
      savingTemplate: false,
      deletingTemplate: false,
      usersAndGroups: false,
      savingSchedule: false,
      users: false,
      workingDays: false
    }
  },
  mutations: {
    updateLoading (state, entity) {
      state.loading[entity.name] = entity.flag
    }
  },
  getters: {
    loadingState (state) {
      return state.loading
    }
  }
}
