define(
  [
    './app.js',
    'jquery',
    './lib/Constants.js',
    './lib/AomAiApi.js',
    './lib/Utils.js',
    'lib/components/base/modal',
    'moment'
  ],
  function (App, $, Constants, AmoAiApi, Utils, modal, moment) {
    return function () {
      const $body = $('body')
      const self = this;
      self.interval = false;
      self.days = [];
      self.hasHiddenUsers = false;
      self.isAssigned = false;

      this.constants = {}
      this.utils = {}
      this.amoAiApi = {}


      this.datesToHide = [];

      //Скрытие нерабочих дней
      this.hide = function () {
        const selector = self.datesToHide.map(d => `[data-date="${d}"]`).join(',')

        setTimeout(() => {
          $('.added').remove();
          $("<span />", {'class': 'amoai_work_schedule_added'}).css({
            position: "absolute",
            class: "added",
            height: "100%",
            left: 0,
            top: 0,
            zIndex: 1000000
          }).appendTo($(selector).css("position", "relative"));
        }, 500);
      };
      //Отбор дат для скрытия
      this.hideDates = function (date) {
        const scheduleDate = self.days.find(x => x.date === date);
        if (scheduleDate === undefined) {
          this.datesToHide.push(date);
        }
      };
      // Получение настроек
      this.getDpSettings = function () {
        self.utils.dp_loader(true);
        const configId = $(self.utils.getWidgetId()).find('input[name="params"]').val();

        self.$authorizedAjax({
          url: self.constants.configuration.APIUrl + '/api/dp/get-config/' + (configId !== '' ? configId : 0),
          method: 'GET',
          dataType: 'json',
          success: function (result) {
            if (result.success) {
              let data = result.payload;
              self.amoAiApi.getPipelines().then(function (pipelinesResult) {
                let items_statuses = [],
                  checkedCount_statuses = 0;

                // Создаем структуру для выбора воронок и статусов, добавляем выборку "Всех"
                $.each(pipelinesResult.response.pipelines, function (pKey, pVal) {
                  items_statuses.push({
                    id: pVal.id + '_all',
                    name: pVal.id + '[]',
                    option: 'Выбрать все',
                    divider_before: {title: pVal.name},
                    divider_after: false,
                    active: 'Y',
                    prefix: pVal.id + '-all',
                    name_is_array: true
                  });

                  $.each(pVal.statuses, function (sKey, sVal) {
                    let is_checked = false;
                    sVal.id = pVal.id + '_' + sVal.id;
                    if ($.inArray(sVal.id, data.statuses) !== -1) {
                      is_checked = true;
                      checkedCount_statuses++;
                    }
                    items_statuses.push({
                      id: sVal.id,
                      name: pVal.id + '[]',
                      option: sVal.name,
                      divider_before: false,
                      divider_after: false,
                      active: 'Y',
                      bg_color: sVal.color,
                      prefix: pVal.id + '-' + sVal.id,
                      is_checked: is_checked,
                      name_is_array: true
                    });
                  });
                });

                self.amoAiApi.getManagers().then(function (managersRes) {
                  let items_users = [],
                    checkedCount_users = 0;

                  // Создаем структуру списка менеджеров
                  $.each(managersRes.groups, function (gKey, gVal) {
                    let firstItem_users = true;

                    $.each(managersRes.managers, function (uKey, uVal) {
                      if (uVal.group == gKey) {
                        let is_checked = false;

                        if ($.inArray(uVal.id, data.users) !== -1) {
                          is_checked = true;
                          checkedCount_users++;
                        }

                        items_users.push({
                          id: uVal.id,
                          name: uVal.id,
                          option: uVal.option,
                          divider_before: firstItem_users != false ? {title: gVal} : false,
                          divider_after: false,
                          active: 'Y',
                          bg_color: '#fff',
                          prefix: gKey + '-' + uVal.id,
                          is_checked: is_checked,
                          name_is_array: true
                        });

                        firstItem_users = false;
                      }
                    });
                  });

                  // Параметры для шаблонизатора
                  let params = {
                    statuses: {
                      items: items_statuses,
                      checked: checkedCount_statuses,
                      total: items_statuses.length
                    },
                    users: {
                      items: items_users,
                      checked: checkedCount_users,
                      total: items_users.length
                    },
                    data: data,
                    statuses_class: self.utils.getWidgetId('', false) + '_statuses',
                    users_class: self.utils.getWidgetId('', false) + '_users',
                    control_leads_id: self.utils.getWidgetId('', false) + '_control_leads',
                    consider_loading: self.utils.getWidgetId('', false) + '_consider_loading',
                    consider_schedule: self.utils.getWidgetId('', false) + '_consider_schedule',
                    consider_tasks: self.utils.getWidgetId('', false) + '_consider_tasks',
                    change_contacts: self.utils.getWidgetId('', false) + '_change_contacts',
                    change_tasks: self.utils.getWidgetId('', false) + '_change_tasks',
                    repeat_distribution_time: self.utils.getWidgetId('', false) + '_repeat_distribution_time',
                    self: self
                  };
                  self.utils.loadTemplates(['settings'], function () {
                    const html = self.constants.configuration.templates['settings'].render(params)
                    $(self.utils.getWidgetId()).find('.settings-loader-wrapper').replaceWith(html);
                  })
                }, function (error) {
                  self.utils.openAlert(self.constants.configuration.error_msg, 'error');
                  self.dp_loader(false);
                });
              }, function (error) {
                self.utils.openAlert(self.constants.configuration.error_msg, 'error');
                self.utils.dp_loader(false);
              });
            } else {
              self.utils.openAlert(result.msg, 'error');
              self.utils.dp_loader(false);
            }
          },
          error: function (jqxhr, status, errorMsg) {
            self.utils.openAlert(self.constants.configuration.error_msg, 'error');
            self.utils.dp_loader(false);
          }
        });
      };
      // Сохранение процесса
      this.saveDpProcess = function (users, statuses) {
        return new Promise(function (succeed, fail) {
          self.$authorizedAjax({
            url: self.constants.configuration.APIUrl + '/api/dp/save-config',
            method: 'POST',
            data: {
              pipeline_id: AMOCRM.data.current_view.pipeline.id,
              process_id: $(self.utils.getWidgetId()).find('input[name="params"]').val(),
              stage_id: $(self.utils.getWidgetId()).closest('.digital-pipeline__item').data('stage-id'),
              control_leads: $(self.utils.getWidgetId() + '_control_leads').val(),
              consider_loading: $(self.utils.getWidgetId() + '_consider_loading').prop('checked') === true ? 1 : 0,
              consider_tasks: $(self.utils.getWidgetId() + '_consider_tasks').prop('checked') === true ? 1 : 0,
              consider_schedule: $(self.utils.getWidgetId() + '_consider_schedule').prop('checked') === true ? 1 : 0,
              change_contacts: $(self.utils.getWidgetId() + '_change_contacts').prop('checked') === true ? 1 : 0,
              change_tasks: $(self.utils.getWidgetId() + '_change_tasks').prop('checked') === true ? 1 : 0,
              repeat_distribution_time: $(self.utils.getWidgetId() + '_repeat_distribution_time').val(),
              users: users,
              statuses: statuses
            },
            dataType: 'json',
            success: function (data) {
              succeed(data);
            },
            error: function (jqxhr, status, errorMsg) {
              fail(new Error("Request failed: " + errorMsg));
            }
          });
        });
      };
      // Сохранение настроек процесса
      this.saveDpSettings = function () {
        self.utils.dp_loader(true);

        return self.amoAiApi.getWidgetStatus().then(function (res) {
          res.data.active = true
          if (typeof res.data.active != 'undefined' && res.data.active === true) {
            let users = [],
              statuses = [];

            // Проверка выбора пользователей
            if (!$('.' + self.utils.getWidgetId('', false) + '_users input.js-item-checkbox:checked').length) {
              self.utils.openAlert('Выберите группу или пользователя', 'error');
              self.utils.dp_loader(false);
              return false;
            }

            // Пользователи
            $('.' + self.utils.getWidgetId('', false) + '_users input.js-item-checkbox').each(function () {
              if ($(this).is(':checked')) {
                users.push($(this).val());
              }
            });

            // Проверка выбора статусов
            if (!$('.' + self.utils.getWidgetId('', false) + '_statuses').closest('.form-group__control').hasClass('amoai-leads_distribution-dp_settings_disable')) {
              if (!$('.' + self.utils.getWidgetId('', false) + '_statuses input.js-item-checkbox:checked').length) {
                self.utils.openAlert('Выберите статусы!', 'error');
                self.utils.dp_loader(false);

                return false;
              }

              // Статусы
              $('.' + self.utils.getWidgetId('', false) + '_statuses input.js-item-checkbox').each(function () {
                if ($(this).is(':checked')) {
                  statuses.push($(this).val());
                }
              });
            }

            return self.saveDpProcess(users, statuses).then(function (res) {
              self.utils.dp_loader(false);
              if (res.success) {
                $(self.utils.getWidgetId()).find('input[name="params"]').val(res.payload.process_id);
              }

              let type = !res.success ? 'error' : 'success';
              self.utils.openAlert(res.payload.msg, type);

              return res.success;
            }, function (error) {
              self.utils.dp_loader(false);
              self.utils.openAlert(self.constants.configuration.error_msg, 'error');

              return false;
            });
          } else {
            self.utils.openAlert(res.data.msg, 'error');
            self.utils.dp_loader(false);
            return false;
          }
        }, function (error) {
          self.utils.dp_loader(false);
          self.utils.openAlert(self.constants.configuration.error_msg, 'error');
          return false;
        });
      };
      // Удаление процесса
      this.deleteDpProcess = function (process_id) {
        return new Promise(function (succeed, fail) {
          self.$authorizedAjax({
            url: self.constants.configuration.APIUrl + '/api/dp/delete',
            method: 'DELETE',
            data: {
              process_id: process_id
            },
            dataType: 'json',
            success: function (data) {
              succeed(data);
            },
            error: function (jqxhr, status, errorMsg) {
              fail(new Error("Request failed: " + errorMsg));
            }
          });
        });
      };
      // Удаление настроек процесса
      this.deleteDpSettings = function () {
        let process_id = $(self.utils.getWidgetId()).find('input[name="params"]').val();

        if (process_id.trim() !== '') {
          self.utils.dp_loader(true);

          return self.deleteDpProcess(process_id).then(function (res) {
            self.utils.dp_loader(false);
            let type = res.success ? 'error' : 'success';
            self.utils.openAlert(res.payload ? res.payload.msg : res.msg, type);
            return res.success;
          }, function (error) {
            self.utils.dp_loader(false);
            self.utils.openAlert(self.constants.configuration.error_msg, 'error');

            return false;
          });
        }
        return true;
      };
      // Отключаем передвигание триггера (draggable)
      this.draggableDisabledToWidget = function (destroyMouseEvents = false) {
        if (destroyMouseEvents) {
          $('body').off( "mouseenter.atws mouseleave.atws mousemove.atws mouseover.atws");
        } else {
          $('body').off( "mouseenter.atws mouseleave.atws mousemove.atws mouseover.atws");
          $('body').on( "mouseenter.atws mouseleave.atws mousemove.atws mouseover.atws", function(event, ui){
            event.preventDefault()
            $(`body .digital-pipeline__business_processes-inner[data-widget-id=${self.params.id}]`).closest('.digital-pipeline__business_processes').off( "dragcreate");
            $(`body .digital-pipeline__business_processes-inner[data-widget-id=${self.params.id}]`).closest('.digital-pipeline__business_processes').on( "dragcreate", function(event, ui){
              event.preventDefault()
              $(event.target).draggable("disable");
            });
            $(`.digital-pipeline__business_processes-inner[data-widget-id=${self.params.id}] .js-copy-trigger`).remove();
          });
        }
      }

      this.callbacks = {
        render() {
          self.constants = new Constants(self)
          self.utils = new Utils(self)
          self.amoAiApi = new AmoAiApi(self);
          self.draggableDisabledToWidget();

          const bodyElement = document.querySelector('body');
          let observer = new MutationObserver(mutationRecords => {
            mutationRecords.forEach(rec => {
              if (!rec.addedNodes.length) {
                return false
              }
              let modal = undefined;

              for (const addedNode of rec.addedNodes) {
                if (addedNode.classList === undefined) {
                  continue;
                }
                if (addedNode.classList.contains("modal-todo")) {
                  modal = addedNode;
                }
              }
              if (modal !== undefined) {
                self.interVal = setInterval(() => {
                  const inputNode = document.querySelector('input[name="main_user"]');
                  if (inputNode !== null) {
                    clearInterval(self.interVal);
                    const $mainUserInput = $('input[name="main_user"]');
                    const $dateInput = $('.tasks-date__caption');
                    const userInit = $mainUserInput.val();
                    self.utils.getScheduleByUser(userInit).then(r => {
                      self.days = r.payload
                    });
                    $mainUserInput.on('change', function () {
                      const user = $(this).val();
                      self.utils.getScheduleByUser(user).then(r => self.days = r.payload)
                    });
                    const el = document.querySelector('.tasks-date__controls-date-input');
                    el.value = '';
                    let as = new MutationObserver(records => {
                      records.forEach(x => {
                        if (x.attributeName !== "from") {
                          return true;
                        }
                        self.datesToHide = [];
                        $('.k-days>span').each(function () {
                          self.hideDates($(this).attr('data-date'));
                        })
                        self.hide();
                      })
                    });
                    as.observe(el, {characterData: false, attributes: true, childList: false, subtree: false});
                  }
                }, 500)
              }
            })
          });
          observer.observe(bodyElement, {
            childList: true,
          })
          return true
        },
        init() {
          // Отключаем копирование триггера
          $(`.digital-pipeline__business_processes-inner[data-widget-id=${self.params.id}] .js-copy-trigger`).remove();
          // Отключаем передвигание триггера (draggable)
          self.draggableDisabledToWidget();

          return true;
        },
        bind_actions() {

          $('body').off('mouseenter', `.digital-pipeline__business_processes-inner[data-widget-id=${self.params.id}]`);
          $('body').on('mouseenter', `.digital-pipeline__business_processes-inner[data-widget-id=${self.params.id}]`, function () {
            $(this).find('.js-copy-trigger').remove();
          });
          self.draggableDisabledToWidget();

          // View "Settings" tab
          $body.off('change', self.utils.getWidgetId() + ' .view-integration-modal__tabs input[name="type_integration_modal"]')
          $body.on('change', self.utils.getWidgetId() + ' .view-integration-modal__tabs input[name="type_integration_modal"]', function () {
            const id = $(self.utils.getWidgetId() + ' .view-integration-modal__tabs input[name="type_integration_modal"]:checked').attr('id')
            if (id === 'setting') {
              $(self.utils.getWidgetId() + ' .view-integration-modal__keys').addClass('hidden')
              $(self.utils.getWidgetId() + ' .view-integration-modal__access').addClass('hidden')
              $(self.utils.getWidgetId() + ' .widget-settings__desc-space').addClass('hidden')
              $(self.utils.getWidgetId() + ' .view-integration-modal__setting').removeClass('hidden')
            } else {
              $(self.utils.getWidgetId() + ' .view-integration-modal__setting').addClass('hidden')
            }
          })
          // Open "OrderForm"
          $body.off('click', `#${self.constants.configuration.ai_widget_code}_buy_widget_btn`)
          $body.on('click', `#${self.constants.configuration.ai_widget_code}_buy_widget_btn`, function (e) {
            e.preventDefault()
            self.utils.openOrderForm()
          });
          // Send "OrderForm"
          $body.off('click', `#${self.constants.configuration.ai_widget_code}_buy_widget_send`)
          $body.on('click', `#${self.constants.configuration.ai_widget_code}_buy_widget_send`, function (e) {
            e.preventDefault()
            self.amoAiApi.sendOrderForm($(this))
          });

          // Выбрать все
          $body.off('change', '.' + self.utils.getWidgetId('', false) + '_statuses .checkboxes_dropdown__label_title_divider_before .js-item-checkbox');
          $body.on('change', '.' + self.utils.getWidgetId('', false) + '_statuses .checkboxes_dropdown__label_title_divider_before .js-item-checkbox', function () {
            var name = $(this).attr('name');

            if ($(self.utils.getWidgetId() + ' .js-item-checkbox[name="' + name + '"]').prop('checked') === true) {
              $(self.utils.getWidgetId() + ' .js-item-checkbox[name="' + name + '"]').prop('checked', true);
            } else {
              $(self.utils.getWidgetId() + ' .js-item-checkbox[name="' + name + '"]').prop('checked', false);
            }
          });

          // Учитывать загрузку
          $body.off('change', self.utils.getWidgetId() + '_consider_loading');
          $body.on('change', self.utils.getWidgetId() + '_consider_loading', function () {
            $('.' + self.utils.getWidgetId('', false) + '_statuses').closest('.form-group__control').toggleClass('amoai-leads_distribution-dp_settings_disable');
          });

          return true
        },
        settings() {
          self.utils.appendCss('style.css');
          self.utils.loadTemplates(self.constants.configuration.settingsTemplatesArray, async function () {
            const $modal = $('.widget-settings__modal.' + self.params.widget_code)
            const $save = $modal.find('button.js-widget-save')
            $modal.attr('id', self.utils.getWidgetId('', false)).addClass('amoai-settings')
            try {
              // const widgetSettings = await self.amoAiApi.getWidgetSettings();
              const widgetSettings = {
                data: {
                  description: "<div>description</div>",
                  footer: "<div>footer</div>",
                  confirm: "<div>confirm</div>",
                  warning: "<div>warning</div>"
                }
              };
              // const panelStatus = await self.amoAiApi.getWidgetStatus();
              const panelStatus = {
                data: {
                  msg: 'msg',
                  active: true,
                  error: false
                }
              };
              // Add description
              $modal.find('.widget_settings_block__descr').html(widgetSettings.data.description);
              // Add footer
              $modal.find('.widget-settings__wrap-desc-space').append(widgetSettings.data.footer);
              // Add confirm
              $modal.find('.widget_settings_block__fields').append(widgetSettings.data.confirm);
              // Add status text
              $modal.find('.amoai_settings_payment_info_text').text(panelStatus.data.msg).css("color", panelStatus.data.active ? "#749e42" : "#ff7779");
              if (self.get_install_status() !== 'installed') {
                if (panelStatus.error) {
                  // Add warning
                  $modal.find('.widget_settings_block__descr').prepend(widgetSettings.data.warning);
                }
                // Activate btn
                $save.find('.button-input-inner__text').text('Активировать виджет');
                $save.trigger('button:enable').addClass('amoai-settings-activate_btn');
                // Автозаполнение телефона
                let is_admin = AMOCRM.constant('managers')[AMOCRM.constant('user').id].is_admin;
                if (is_admin === 'Y') {
                  if ($modal.find('input[name="phone"]').val() === '' && AMOCRM.constant('user').personal_mobile !== '') {
                    $modal.find('input[name="phone"]').val(AMOCRM.constant('user').personal_mobile);
                  }
                }
                // Активировать виджет
                let save_flag = false;
                $save.off('click').on('click', function () {
                  if (save_flag) {
                    save_flag = false;
                    return true;
                  }
                  // Если не заполнено поле "Телефон"
                  if ($modal.find('input[name="phone"]').val() === '') {
                    self.utils.openAlert('Заполните номер телефона!', 'error');
                    return false;
                  }
                  // Если не дали согласие на передачу данных
                  if ($modal.find('#amoai_settings_confirm').prop('checked') === false) {
                    self.utils.openAlert('Вам необходимо дать согласие на передачу данных из amoCRM.', 'error');
                    return false;
                  }
                  self.amoAiApi.installWidget($save).then(res => {
                    let type = res.error ? 'error' : 'success';
                    self.utils.openAlert(res.msg, type);
                    if (type === 'success') {
                      save_flag = true;
                      $save.trigger('click');
                    }
                  }).catch(error => {
                    self.utils.openAlert(self.constants.configuration.error_msg, 'error');
                  });
                  return true;
                });
                return true;
              }
              // Deactive btn & hide warning
              $modal.find('.widget_settings_block__fields').hide();
              if (panelStatus.data.active) {
                if (self.constants.configuration.hasSettingsTab) {
                  const tabParams = {
                    id: 'setting',
                    text: 'Настройки'
                  }
                  const tabHtml = self.constants.configuration.templates[self.constants.configuration.tabItemTemplate].render({
                    tab: tabParams
                  })
                  //$modal.find('.view-integration-modal__tabs .tabs').append(tabHtml)
                  const $modalBody = $modal.find('.modal-body')
                  $modalBody.width($modalBody.width() + $('#setting').width() + 40)
                  $modalBody.trigger('modal:centrify')
                  //const tabContentHtml = self.constants.configuration.templates[self.constants.configuration.tabContentTemplate].render({tab: tabParams})
                  //$modal.find('.widget-settings__desc-space').before(tabContentHtml)
                  $('.widget-settings').css('overflow', 'unset');
                  //self.utils.initializeVue(App, '.view-integration-modal__' + tabParams.id);
                  //self.utils.appendCss('https://unpkg.com/element-ui/lib/theme-chalk/index.css', true)
                }
              }
            } catch (e) {
              self.utils.openAlert(self.constants.configuration.error_msg, 'error');
            }
          })
        },
        dpSettings: function () {
          self.utils.appendCss('style.css');

          const $save = $('button.js-trigger-save');
          const $cancel = $('button.js-trigger-cancel');
          const $delete = $('svg.digital-pipeline__edit-delete');
          const $modal = $save.closest('[data-action=send_widget_hook]');

          $modal.attr('id', self.utils.getWidgetId('', false)).addClass('amoai-leads_distribution-dp_settings');
          $modal.find('.digital-pipeline__edit-forms .task-edit__body__form').hide();
          $modal.find('.task-edit__body__form').after('<div class="amoai-leads_distribution-dp_settings_container"></div>');

          // Получить настройки
          self.getDpSettings();

          // Сохранить настройки процесса
          let save_flag = false;
          $save.off("click");
          $save.on('click', function () {
            if (save_flag) {
              save_flag = false;
              return true;
            }
            self.saveDpSettings().then(function (result) {
              if (result === true) {
                save_flag = true;
                $save.trigger('click');
              }
            });
            return false;
          });

          // Удалить настройки процесса
          let delete_flag = false;
          $delete.off('click');
          $delete.on('click', function () {
            if (delete_flag) {
              delete_flag = false;
              return true;
            }
            self.deleteDpSettings().then(function (result) {
              if (result === true) {
                delete_flag = true;
                $delete.trigger('click');
              }
            });

            return false;
          });

          return true;
        },
        onSave(fields) {
          return true
        },
        destroy() {
          self.draggableDisabledToWidget(true);
          return true
        },
        contacts: {
          selected() {

          }
        },
        leads: {
          selected() {

          }
        },
        tasks: {
          selected() {

          }
        },
        advancedSettings() {
          return true;
        }
      }
      return this
    }
  })
