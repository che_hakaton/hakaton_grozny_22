// Утилиты
define(['lib/components/base/modal'],
  function (Modal) {
    return function ({constants}) {
      const self = this
      this.getWidgetId = function (postfix, hash) {
        hash = typeof hash !== 'undefined' ? hash : true
        postfix = typeof postfix !== 'undefined' ? postfix : ''
        return (hash ? '#' : '') + constants.widget.params.widget_code + (postfix ? '_' + postfix : '')
      };
      this.openNotifications = function (text, type = 'success') {
        const params = {
          header: constants.configuration.ai_widget_name,
          text: text
        }
        if (type === 'success') {
          AMOCRM.notifications.show_message(params)
          return true
        }
        if (type === 'error') {
          AMOCRM.notifications.show_message_error(params)
          return true
        }
        return false
      };
      this.openAlert = function (text, type = 'success') {
        if (type === 'success') {
          return new Modal()._showSuccess(text, false, 3000)
        }
        if (type === 'error') {
          return new Modal()._showError(text, false)
        }
        return false
      };
      this.openModal = function (data, class_name) {
        constants.widget.openedModal = new Modal({
          class_name: 'modal-list ' + class_name,
          init: function ($modalBody) {
            $modalBody
              .trigger('modal:loaded')
              .html(data)
              .trigger('modal:centrify')
              .append('<span class="modal-body__close"><span class="icon icon-modal-close"></span></span>')
          },
          destroy: function () {
            constants.widget.openedModal = false
          }
        })
      };
      this.openOrderForm = function () {
        const html = constants.configuration.templates.order_form.render({
          btn_id: `${constants.configuration.ai_widget_code}_buy_widget_send`,
          btn_text: 'Оставить заявку',
          name: AMOCRM.constant('user').name,
          phone: AMOCRM.constant('user').personal_mobile,
          email: AMOCRM.constant('user').login,
          self: constants.widget
        })
        self.openModal(html, 'amoai-settings-order_form')
      };
      this.appendCss = function (file, fromCdn = false) {
        if (!fromCdn){
          if ($(`link[href="${constants.configuration.base_path}/css/${file}?v=${constants.widget.params.version}"]`).length) {
            return false
          }
          $('head').append(`<link type="text/css" rel="stylesheet" href="${constants.configuration.base_path}/css/${file}?v=${constants.widget.params.version}">`)
          return true
        }
        $('head').append(`<link type="text/css" rel="stylesheet" href="${file}">`)
      };
      this.loader = function (querySelector, show = false, refresh = false) {
        if (show === true) {
          if (refresh) {
            $(querySelector).html(
              '<div class="overlay-loader">' +
              '<span class="spinner-icon spinner-icon-abs-center"></span>' +
              '</div><br>'
            )
            return true
          }
          $(querySelector).prepend(
            '<div class="overlay-loader">' +
            '<span class="spinner-icon spinner-icon-abs-center"></span>' +
            '</div><br>'
          )
          return true
        }
        $(querySelector).find('.overlay-loader').remove()
        return true
      };
      this.loadTemplates = function (templates, callback) {
        const templateName = templates.shift()
        if (templateName === undefined) {
          callback()
          return
        }
        if (constants.configuration.templates[templateName] !== undefined) {
          self.loadTemplates(templates, callback)
          return
        }
        // noinspection JSUnusedGlobalSymbols
        constants.widget.render({
          href: `/templates/${templateName}.twig`,
          base_path: constants.configuration.base_path,
          load(template) {
            constants.configuration.templates[templateName] = template
            self.loadTemplates(templates, callback)
          }
        }, {})
      };
      this.initializeVue = function (VueApp, parentBLockSelector, condition = true) {
        $(parentBLockSelector).append(`<div id="${constants.widget.params.widget_code}_vue_app"></div>`)
        if (condition) {
          self.appendCss('app.css')
          VueApp.default.render(constants.widget, `#${constants.widget.params.widget_code}_vue_app`, AMOCRM.constant())
        }
      };
      //
      this.getScheduleByUser = function (userId) {
        return new Promise(function (succeed, fail) {
          constants.widget.$authorizedAjax({
            url: constants.configuration.APIUrl + '/api/users/get-working-days',
            type: 'get',
            dataType: 'json',
            data: {
              user: userId
            },
            success: function (data) {
              succeed(data)
            },
            error: function (jqxhr, status, errorMsg) {
              fail(new Error('Request failed: ' + errorMsg))
            }
          })
        })
      };
      //
      this.centrifyModal = function () {
        $('.modal-body').trigger('modal:centrify')
      };
      // Загрузка
      this.dp_loader = function (show) {
        show = typeof show !== 'undefined' ? show : true;
        if (show) {
          if (!$(self.getWidgetId()).find('.amoai-leads_distribution-dp_settings_container .overlay-loader').length) {
            $(self.getWidgetId()).find('.amoai-leads_distribution-dp_settings_container').append(
              '<div class="settings-loader-wrapper">\
                          <span class="pipeline_leads__load_more__spinner spinner-icon spinner-icon-abs-center settings-spinner"></span>\
                       </div>'
            );
          }
        } else {
          $(self.getWidgetId()).find('.amoai-leads_distribution-dp_settings_container .settings-loader-wrapper').remove();
        }
      };
      return this
    }
  });
