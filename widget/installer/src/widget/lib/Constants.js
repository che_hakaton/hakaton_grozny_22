define([], function () {
  return function (widget){
    this.tabItemTemplate = 'tab_item'
    this.tabContentTemplate = 'tab_content'
    const isProd = true;
    this.isServerTemplatePath = true;
    this.APIUrl = isProd ? 'https://fx.amoai.ru' : 'https://wsh.sverka95.ru'
    const self = this
    this.widget = widget;
    this.configuration = {
      widget: self.widget,
      PanelAPIUrl: 'https://amoai.ru/pm',
      ai_widget_name: 'voximplant',
      ai_widget_code: 'voximplant',
      base_path: this.isServerTemplatePath ? this.APIUrl + '/widget' : self.widget.params.path,
      error_msg: 'Произошла ошибка на стороне сервера! Обратитесь в службу технической поддержки виджета.',
      hasSettingsTab: true,
      settingsTemplatesArray: [self.tabContentTemplate, self.tabItemTemplate, 'order_form'],
      activeUsers: Object.values(AMOCRM.constant('managers')).filter(x => x.active).length,
      templates: {},
      APIUrl:self.APIUrl,
      tabContentTemplate:self.tabContentTemplate,
      tabItemTemplate:self.tabItemTemplate
    };
    return this
  }
})
