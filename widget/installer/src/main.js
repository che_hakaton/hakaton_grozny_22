import Vue from 'vue'
import App from './App.vue'
import store from './store'
import {
  Alert,
  Button,
  Calendar,
  CheckboxButton,
  CheckboxGroup,
  Col,
  DatePicker,
  Form,
  FormItem,
  Icon,
  Input,
  Loading,
  Message,
  MessageBox,
  Notification,
  Option,
  OptionGroup,
  Row,
  Collapse,
  CollapseItem,
  Select,
  Switch,
  Table,
  TableColumn,
  TabPane,
  Tabs,
  TimeSelect,
  Popover,
  Checkbox
} from 'element-ui'
import lang from 'element-ui/lib/locale/lang/ru-RU'
import locale from 'element-ui/lib/locale'
import { setUpAmoConstant, setUpWidget } from '@/constants'

// configure language
locale.use(lang)
Vue.use(Input)
Vue.use(CheckboxButton)
Vue.use(CheckboxGroup)
Vue.use(Switch)
Vue.use(Select)
Vue.use(Option)
Vue.use(OptionGroup)
Vue.use(Button)
Vue.use(Table)
Vue.use(TableColumn)
Vue.use(DatePicker)
Vue.use(TimeSelect)
Vue.use(Form)
Vue.use(FormItem)
Vue.use(Tabs)
Vue.use(TabPane)
Vue.use(Alert)
Vue.use(Icon)
Vue.use(Row)
Vue.use(Col)
Vue.use(Calendar)
Vue.use(Popover)
Vue.use(Collapse)
Vue.use(CollapseItem)
Vue.use(Checkbox)

Vue.use(Loading.directive)

Vue.prototype.$loading = Loading.service
Vue.prototype.$msgbox = MessageBox
Vue.prototype.$alert = MessageBox.alert
Vue.prototype.$confirm = MessageBox.confirm
Vue.prototype.$prompt = MessageBox.prompt
Vue.prototype.$notify = Notification
Vue.prototype.$message = Message

Vue.config.productionTip = false

export default {
  render (widget, selector, constant) {
    setUpWidget(widget)
    setUpAmoConstant(constant)
    if (window[widget.params.widget_code] === undefined) {
      window[widget.params.widget_code] = {}
    }
    window[widget.params.widget_code].vue = new Vue({
      store,
      render: (h) => h(App)
    }).$mount(selector)
  }
}
