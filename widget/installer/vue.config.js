const path = require('path')
const AmdWebpackPlugin = require('amd-webpack-plugin')
const ZipFilesPlugin = require('webpack-zip-files-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')

module.exports = {
  // publicPath: process.env.VUE_APP_COMPONENTS_URL,
  outputDir: '../../app/public/widget',
  productionSourceMap: false,
  configureWebpack: {
    output: {
      libraryTarget: 'amd',
      filename: 'app.js'
    },
    optimization: {
      nodeEnv: 'production',
      minimize: true
    },
    plugins: [
      new AmdWebpackPlugin(),
      new CopyWebpackPlugin({
        patterns: [
          {
            from: 'src/widget/',
            to: '.'
          }
        ]
      }),
      new ZipFilesPlugin({
        entries: [
          {
            src: '../../app/public/widget',
            dist: '/'
          }
        ],
        output: path.join(__dirname, './publish/widget'),
        format: 'zip'
      })
    ]
  },
  filenameHashing: false,
  chainWebpack: (config) => {
    config.plugins.delete('html')
    config.plugins.delete('preload')
    config.plugins.delete('prefetch')
    config.optimization.delete('splitChunks')
    const types = ['vue-modules', 'vue', 'normal-modules', 'normal']
    types.forEach((type) =>
      addStyleResource(config.module.rule('stylus').oneOf(type))
    )
  }
}

function addStyleResource (rule) {
  rule
    .use('style-resource')
    .loader('style-resources-loader')
    .options({
      patterns: [path.resolve(__dirname, './src/styles/imports.styl')]
    })
}
