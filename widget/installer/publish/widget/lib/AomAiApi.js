define([], function () {
    return function ({constants, utils}) {
      self = this;
      // Установка виджета
      this.installWidget = function (btn) {
        return new Promise(function (succeed, fail) {
          btn.trigger('button:load:start')
          $.ajax({
            url: constants.configuration.PanelAPIUrl + '/install',
            type: 'post',
            data: {
              // Widget
              widget_code: constants.configuration.ai_widget_code,

              // Account
              account_id: AMOCRM.constant('account').id,
              license_date: AMOCRM.constant('account').paid_till,
              tariff: AMOCRM.constant('account').tariffName,
              users: constants.configuration.activeUsers,

              // User
              client_id: AMOCRM.constant('user').id,
              name: AMOCRM.constant('user').name,
              email: AMOCRM.constant('user').login,
              profile_phone: AMOCRM.constant('user').personal_mobile,
              phone: $('.amoai-pm-settings.' + constants.widget.params.widget_code).find('input[name=phone]').val()
            },
            dataType: 'json',
            success: function (data) {
              succeed(data)
            },
            error: function (jqxhr, status, errorMsg) {
              fail(new Error('Request failed: ' + errorMsg))
            }
          })
        })
      };
      // Отправка формы "Купить"
      this.sendOrderForm = function (btn) {
        const form = $('.amoai-settings-order_form form')
        let error = false
        form.find('.required').each(function () {
          if ($(this).val() === '') {
            $(this).addClass('error')
            error = true
            utils.openAlert('Заполните обязательные поля!', 'error')
            return true
          }
          $(this).removeClass('error')
        })
        if (error === false) {
          btn.trigger('button:load:start')
          $.ajax({
            url: constants.configuration.PanelAPIUrl + '/order',
            type: 'post',
            data: {
              // Widget
              widget_code: constants.configuration.ai_widget_code,
              // Account
              account_id: AMOCRM.constant('account').id,
              license_date: AMOCRM.constant('account').paid_till,
              tariff: AMOCRM.constant('account').tariffName,
              users: constants.configuration.activeUsers,

              // User
              client_id: AMOCRM.constant('user').id,
              name: form.find('input[name=name]').val(),
              phone: form.find('input[name=phone]').val(),
              email: form.find('input[name=email]').val(),
              comment: form.find('textarea[name=comment]').val(),
              profile_phone: AMOCRM.constant('user').personal_mobile
            },
            dataType: 'json',
            success: function (res) {
              const type = res.error ? 'error' : 'success'
              utils.openAlert(res.msg, type)
              btn.closest('.modal').remove()
              btn.trigger('button:load:stop')
            },
            error: function () {
              utils.openAlert(constants.configuration.error_msg, 'error')
              btn.trigger('button:load:stop')
            }
          })
        }
      };
      // Получение статуса виджета
      this.getWidgetStatus = function () {
        return new Promise(function (succeed, fail) {
          $.ajax({
            url: constants.configuration.PanelAPIUrl + '/status',
            type: 'post',
            data: {
              account_id: AMOCRM.constant('account').id,
              widget_code: constants.configuration.ai_widget_code
            },
            dataType: 'json',
            success: function (data) {
              succeed(data)
            },
            error: function (jqxhr, status, errorMsg) {
              fail(new Error('Request failed: ' + errorMsg))
            }
          })
        })
      };
      // Получение настроек виджета
      this.getWidgetSettings = function () {
        return new Promise(function (succeed, fail) {
          $.ajax({
            url: 'https://amoai.ru/pm/market-place/widget/' + constants.configuration.ai_widget_code + '/settings',
            type: 'get',
            dataType: 'json',
            success: function (data) {
              succeed(data)
            },
            error: function (jqxhr, status, errorMsg) {
              fail(new Error('Request failed: ' + errorMsg))
            }
          })
        })
      };
      //Получение воронок
      this.getPipelines = function () {
        return new Promise(function (succeed, fail) {
          $.ajax({
            url: "/ajax/v1/pipelines/list",
            type: "GET",
            dataType: "json",
            success: function (data) {
              succeed(data);
            },
            error: function (jqxhr, status, errorMsg) {
              fail(new Error("Request failed: " + errorMsg));
            }
          });
        });
      };
      // Получение пользователей
      this.getManagers = function () {
        return new Promise(function (succeed, fail) {
          $.ajax({
            url: "/ajax/get_managers_with_group/",
            type: "POST",
            dataType: "json",
            success: function (data) {
              succeed(data);
            },
            error: function (jqxhr, status, errorMsg) {
              fail(new Error("Request failed: " + errorMsg));
            }
          });
        });
      };
    }
  }
)
