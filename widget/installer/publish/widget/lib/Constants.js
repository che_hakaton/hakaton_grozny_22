define([], function () {
  return function (widget){
    this.tabItemTemplate = 'tab_item'
    this.tabContentTemplate = 'tab_content'
    this.APIUrl = 'https://ws.amoai.ru'
    // this.APIUrl = 'https://wsh.sverka95.ru'
    const self = this
    this.widget = widget;
    this.configuration = {
      widget: self.widget,
      PanelAPIUrl: 'https://amoai.ru/pm',
      ai_widget_name: 'work_schedule',
      ai_widget_code: 'work_schedule',
      base_path: self.widget.params.path,
      // base_path: this.APIUrl + '/widget',
      error_msg: 'Произошла ошибка на стороне сервера! Обратитесь в службу технической поддержки виджета.',
      hasSettingsTab: true,
      settingsTemplatesArray: [self.tabContentTemplate, self.tabItemTemplate, 'order_form'],
      activeUsers: Object.values(AMOCRM.constant('managers')).filter(x => x.active).length,
      templates: {},
      APIUrl:self.APIUrl,
      tabContentTemplate:self.tabContentTemplate,
      tabItemTemplate:self.tabItemTemplate
    };
    return this
  }
})
