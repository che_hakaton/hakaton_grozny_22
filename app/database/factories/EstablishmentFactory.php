<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class EstablishmentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'img' => $this->faker->imageUrl,
            'company_name' => $this->faker->company(),
            'date_registration' => $this->faker->date(),
            'company_site' => $this->faker->url(),
            'description' => $this->faker->text(),
        ];
    }
}
