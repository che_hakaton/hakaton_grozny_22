<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFoodCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('food_categories', function (Blueprint $table) {
            // Многие ко Многие
            // Указать уровень вложенности категории и создать таблица (или без него сделать отношения) связей с ними что бы вытащить их
            $table->id();
            $table->bigInteger('sort')->default(0);
            $table->string('alias')->unique();
            $table->bigInteger('level')->default(0);
            $table->string('name');
            $table->string('img')->nullable();
            $table->longText('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('food_categories');
    }
}
