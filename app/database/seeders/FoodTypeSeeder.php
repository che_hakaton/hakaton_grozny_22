<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FoodTypeSeeder extends Seeder
{
    /**
     * php artisan db:seed --class=FoodTypeSeeder
     *
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('food_types')->insert([
            ['id' => 1, 'sort' => 1,  'alias' => 'vostochnayakyhnya', 'name' => 'Восточная Кухня'],
            ['id' => 3, 'sort' => 3,  'alias' => 'kafe', 'name' => 'Кафе'],
            ['id' => 4, 'sort' => 4,  'alias' => 'zavtrak', 'name' => 'Завтрак'],
            ['id' => 5, 'sort' => 5,  'alias' => 'obed', 'name' => 'Обед'],
            ['id' => 6, 'sort' => 6,  'alias' => 'ujin', 'name' => 'Ужин'],
            ['id' => 7, 'sort' => 7,  'alias' => 'manty', 'name' => 'Манты'],
            ['id' => 8, 'sort' => 8,  'alias' => 'steyk', 'name' => 'Стейк'],
            ['id' => 9, 'sort' => 9,  'alias' => 'shashlik', 'name' => 'Шашлык'],
            ['id' => 10, 'sort' => 10, 'alias' => 'plov', 'name' => 'Плов'],
            ['id' => 11, 'sort' => 11, 'alias' => 'hachapury', 'name' => 'Хачапури'],
            ['id' => 12, 'sort' => 12, 'alias' => 'hinkal', 'name' => 'Хинкали'],
            ['id' => 13, 'sort' => 13, 'alias' => 'bliny', 'name' => 'Блины'],
            ['id' => 14, 'sort' => 14, 'alias' => 'samsa', 'name' => 'Самса'],
            ['id' => 15, 'sort' => 15, 'alias' => 'riba', 'name' => 'Рыба'],
            ['id' => 16, 'sort' => 16, 'alias' => 'cezar', 'name' => 'Цезарь'],
            ['id' => 17, 'sort' => 17, 'alias' => 'myasopofrancyzki', 'name' => 'Мясо по Французски'],
            ['id' => 18, 'sort' => 18, 'alias' => 'hash', 'name' => 'Хаш'],
            ['id' => 19, 'sort' => 19, 'alias' => 'tindirsamsa', 'name' => 'Тандыр-Самса'],
            ['id' => 20, 'sort' => 20, 'alias' => 'kurinauglyah', 'name' => 'Курица на углях'],
            ['id' => 21, 'sort' => 21, 'alias' => 'chepubeli', 'name' => 'Чепубели'],
            ['id' => 22, 'sort' => 22, 'alias' => 'forel', 'name' => 'Форель'],
        ]);
    }
}
