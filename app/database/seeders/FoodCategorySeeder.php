<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FoodCategorySeeder extends Seeder
{
    /**
     * php artisan db:seed --class=FoodCategorySeeder
     *
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('food_categories')->insert(
            [
                ['id'=>1, 'sort'=>1,  'alias' => 'dostavka', 'level' => 1, 'name' => 'Доставка'],
                ['id'=>2, 'sort'=>2,  'alias' => 'kafe', 'level' => 1, 'name' => 'Кафе'],
                ['id'=>3, 'sort'=>3,  'alias' => 'stolovie', 'level' => 1, 'name' => 'Столовые'],
                ['id'=>4, 'sort'=>4,  'alias' => 'kuhny', 'level' => 1, 'name' => 'Кухни'],
                ['id'=>5, 'sort'=>5,  'alias' => 'restorany', 'level' => 1, 'name' => 'Рестораны'],

                ['id'=>6, 'sort'=>6,  'alias' => 'bappa', 'level' => 2, 'name' => 'Bappa.ru'],
                ['id'=>7, 'sort'=>7,  'alias' => 'merzayu', 'level' => 2, 'name' => 'Мерза Ю'],
                ['id'=>8, 'sort'=>8,  'alias' => 'hingalchepalg', 'level' => 2, 'name' => 'Х1ингалш и Ч1епалгш'],
                ['id'=>9, 'sort'=>9,  'alias' => 'vostochnaya', 'level' => 2, 'name' => 'Восточная'],
                ['id'=>10, 'sort'=>10, 'alias' => 'evropeyskaya', 'level' => 2, 'name' => 'Европейская'],
                ['id'=>11, 'sort'=>11, 'alias' => 'nacionalnaya', 'level' => 2, 'name' => 'Национальная'],
                ['id'=>12, 'sort'=>12, 'alias' => 'fastfood', 'level' => 2, 'name' => 'FastFood'],

                ['id'=>13, 'sort'=>13, 'alias' => 'gruzinskaya', 'level' => 3, 'name' => 'Грузинская'],
                ['id'=>14, 'sort'=>14, 'alias' => 'osetinskaya', 'level' => 3, 'name' => 'Осетинская'],
                ['id'=>15, 'sort'=>15, 'alias' => 'chechenskaya', 'level' => 3, 'name' => 'Чеченская'],
                ['id'=>16, 'sort'=>16, 'alias' => 'russkaya', 'level' => 3, 'name' => 'Русская'],
                ['id'=>17, 'sort'=>17, 'alias' => 'makovskiy', 'level' => 3, 'name' => 'Маковский'],
                ['id'=>18, 'sort'=>18, 'alias' => 'crispy', 'level' => 3, 'name' => 'Crispy'],
                ['id'=>19, 'sort'=>19, 'alias' => 'blackstartburger', 'level' => 3, 'name' => 'Blackstar Burger'],
            ]
        );

        DB::table('food_category_food_category')->insert([
            ['id' => 1, 'parent_id' => 1, 'child_id' => 6],
            ['id' => 2, 'parent_id' => 1, 'child_id' => 7],
            ['id' => 3, 'parent_id' => 1, 'child_id' => 8],

            ['id' => 4, 'parent_id' => 2, 'child_id' => 9],
            ['id' => 5, 'parent_id' => 2, 'child_id' => 10],
            ['id' => 6, 'parent_id' => 2, 'child_id' => 11],
            ['id' => 7, 'parent_id' => 2, 'child_id' => 12],

            ['id' => 8, 'parent_id' => 11, 'child_id' => 13],
            ['id' => 9, 'parent_id' => 11, 'child_id' => 14],
            ['id' => 10, 'parent_id' => 11, 'child_id' => 15],
            ['id' => 11, 'parent_id' => 11, 'child_id' => 16],

            ['id' => 12, 'parent_id' => 12, 'child_id' => 17],
            ['id' => 13, 'parent_id' => 12, 'child_id' => 18],
            ['id' => 14, 'parent_id' => 12, 'child_id' => 19],
        ]);
    }
}
