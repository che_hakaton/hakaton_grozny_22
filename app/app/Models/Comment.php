<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;

    public function user(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function establishment(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Establishment::class, 'id', 'establishment_id');
    }

    public function rankings(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Ranking::class, 'comment_id', 'id');
    }
}
