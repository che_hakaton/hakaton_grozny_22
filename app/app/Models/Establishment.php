<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Establishment extends Model
{
    use HasFactory;

    public function food_types(): \Illuminate\Database\Eloquent\Relations\MorphToMany
    {
        return $this->morphToMany(FoodType::class, 'food_typegables');
    }

    public function comments(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Comment::class, 'establishment_id', 'id');
    }

    public function food_categories(): BelongsToMany
    {
        return $this->belongsToMany(FoodCategory::class, 'food_category_establishment', 'food_category_id', 'establishment_id');
    }

    public function getOwnerCategories()
    {
        $categories = '';
        if ($this->food_categories) {

            foreach ($this->food_categories as $foodCategory) {
                $catTemp = $foodCategory;
                while (!empty($catTemp) && $catTemp->level >= 1) {
                    if ($catTemp->level == 1) {
                        $categories .= ' ' . $catTemp->alias;
                        break;
                    }
                    $catTemp = $catTemp->parent;
                }
            }
        }
        return $categories;
    }
}
