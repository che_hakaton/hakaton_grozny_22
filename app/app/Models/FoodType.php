<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FoodType extends Model
{
    use HasFactory;

    public function establishments(): \Illuminate\Database\Eloquent\Relations\MorphToMany
    {
        return $this->morphedByMany(Establishment::class, 'food_typegables');
    }

    public function food_category(): \Illuminate\Database\Eloquent\Relations\MorphToMany
    {
        return $this->morphedByMany(FoodCategory::class, 'food_typegables');
    }

}
