<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class FoodCategory extends Model
{
    use HasFactory;

    public function children(): BelongsToMany
    {
        return $this->belongsToMany(FoodCategory::class, 'food_category_food_category', 'parent_id', 'child_id');
    }

    public function parents(): BelongsToMany
    {
        return $this->belongsToMany(FoodCategory::class, 'food_category_food_category', 'child_id', 'parent_id');
    }

    public function food_types(): \Illuminate\Database\Eloquent\Relations\MorphToMany
    {
        return $this->morphToMany(FoodType::class, 'food_typegables');
    }

    public function establishments(): BelongsToMany
    {
        return $this->belongsToMany(Establishment::class, 'food_category_establishment', 'establishment_id', 'food_category_id');
    }

}
