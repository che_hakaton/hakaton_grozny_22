<?php

namespace App\Http\Controllers;

use App\Models\Establishment;

class Home extends Controller
{
    public function index()
    {
        $allEstablishments = Establishment::query()
            ->with(['food_categories.parents','food_categories.parents.parents','food_categories.parents.parents.parents', 'food_types'])
            ->get();

        return view('welcome', ['allestablishments' => $allEstablishments]);
    }
}
