<?php

use App\Http\Controllers\Home;
use App\Models\Comment;
use App\Models\Establishment;
use App\Models\User;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [Home::class, 'index'])->name('welcome');

Route::get('/card', function () {
    return view('card');
})->name('card');

Route::get('/about', function () {
    return view('about');
})->name('about');

Route::get('/mailru-domainOsorJuL6J85NuDRa.html', function () {
    return 'mailru-domain: OsorJuL6J85NuDRa';
});

Route::get('/dashboard', function () {
    return redirect('/');
//    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';
