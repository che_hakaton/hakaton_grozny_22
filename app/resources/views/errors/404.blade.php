<x-error-layout>
    <section class="home-section home-parallax home-fade home-full-height bg-dark bg-dark-30" id="home" data-background="{{ asset('assets/images/section-4.jpg') }}">
        <div class="titan-caption">
            <div class="caption-content">
                <div class="font-alt mb-30 titan-title-size-4">Ошибка 404 :(</div>
                <div class="font-alt">Данный URL устарел или<br/>Страницы никогда не существовало!
                </div>
                <div class="font-alt mt-30"><a class="btn btn-border-w btn-round" href="{{ url('/')  }}">Вернуться</a></div>
            </div>
        </div>
    </section>
</x-error-layout>

