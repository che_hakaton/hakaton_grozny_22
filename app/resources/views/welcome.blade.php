@extends("layouts.app")

@section("content")
    <section class="module bg-dark-60 portfolio-page-header" data-background="assets/images/section-12.jpg">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <h2 class="module-title font-alt">Только лучшая еда - только качественное обслуживание</h2>
                    <div class="module-subtitle font-serif">Наш сервис основан на опыте многочисленных пользователей и направлен на формирование вашего мнения для вашей пользы и хорошего натсроения!</div>
                </div>
            </div>
        </div>
    </section>
    <section class="module">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ul class="filter font-alt" id="filters">

                        <li><a class="current wow fadeInUp" href="#" data-filter="*">Всё</a></li>
                        @php
                            $delay = 0;
                        @endphp
                        @foreach($topmenus as $topmenu)
                            <li><a class="wow fadeInUp" href="#" data-filter=".{{ $topmenu->alias }}" data-wow-delay="0.{{$delay += 2}}s">{{ $topmenu->name }}</a></li>
                        @endforeach
                        {{--<li><a class="wow fadeInUp" href="#" data-filter=".dostavka" data-wow-delay="0.2s">Доставки</a></li>
                        <li><a class="wow fadeInUp" href="#" data-filter=".kafe" data-wow-delay="0.4s">Кафе</a></li>
                        <li><a class="wow fadeInUp" href="#" data-filter=".stolovie" data-wow-delay="0.6s">Столовые</a></li>
                        <li><a class="wow fadeInUp" href="#" data-filter=".kuhni" data-wow-delay="0.6s">Кухни</a></li>
                        <li><a class="wow fadeInUp" href="#" data-filter=".restoran" data-wow-delay="0.6s">Рестораны</a></li>--}}
                    </ul>
                </div>
            </div>
            @foreach($allestablishments as $allestablishment)
                <div></div>
            @endforeach
            <ul class="works-grid works-grid-masonry works-hover-w works-grid-4" id="works-grid">
                <li class="work-item dostavka"><a href="portfolio_single_featured_image1.html">
                        <div class="work-image"><img src="{{ asset('assets/images/fustfood.jpg') }}" alt="Portfolio Item"/></div>
                        <div class="work-caption font-alt">
                            <h3 class="work-title">Bappa.ru</h3>
                            <div class="work-descr">Доставка</div>
                        </div></a></li>
                <li class="work-item kafe"><a href="portfolio_single_featured_image2.html">
                        <div class="work-image"><img src="{{ asset('assets/images/caffe.jpg') }}" alt="Portfolio Item"/></div>
                        <div class="work-caption font-alt">
                            <h3 class="work-title">Кафе «Орга»</h3>
                            <div class="work-descr">Кафе</div>
                        </div></a></li>
                <li class="work-item dostavka"><a href="portfolio_single_featured_slider1.html">
                        <div class="work-image"><img src="{{ asset('assets/images/chech4.jpg') }}" alt="Portfolio Item"/></div>
                        <div class="work-caption font-alt">
                            <h3 class="work-title">Мерза Ю</h3>
                            <div class="work-descr">Доставка</div>
                        </div></a></li>
                <li class="work-item stolovie"><a href="portfolio_single_featured_image2.html">
                        <div class="work-image"><img src="{{ asset('assets/images/stolovka2.jpg') }}" alt="Portfolio Item"/></div>
                        <div class="work-caption font-alt">
                            <h3 class="work-title">«Другой мир»</h3>
                            <div class="work-descr">Столовая</div>
                        </div></a></li>
                <li class="work-item kafe"><a href="portfolio_single_featured_slider2.htmll">
                        <div class="work-image"><img src="{{ asset('assets/images/caffe2.jpg') }}" alt="Portfolio Item"/></div>
                        <div class="work-caption font-alt">
                            <h3 class="work-title">Жижиг & Galnash</h3>
                            <div class="work-descr">Кафе</div>
                        </div></a></li>
                <li class="work-item dostavka"><a href="portfolio_single_featured_video1.html">
                        <div class="work-image"><img src="{{ asset('assets/images/chech2.jpg') }}" alt="Portfolio Item"/></div>
                        <div class="work-caption font-alt">
                            <h3 class="work-title">Х1ингалш & Ч1епалгш</h3>
                            <div class="work-descr">Доставка</div>
                        </div></a></li>
                <li class="work-item kafe"><a href="portfolio_single_featured_video2.html">
                        <div class="work-image"><img src="{{ asset('assets/images/caffe3.jpg') }}" alt="Portfolio Item"/></div>
                        <div class="work-caption font-alt">
                            <h3 class="work-title">Шашлык Машлык</h3>
                            <div class="work-descr">Кафе</div>
                        </div></a></li>
                <li class="work-item stolovie"><a href="portfolio_single_featured_image2.html">
                        <div class="work-image"><img src="{{ asset('assets/images/stolovka.jpg') }}" alt="Portfolio Item"/></div>
                        <div class="work-caption font-alt">
                            <h3 class="work-title">«Райдан»</h3>
                            <div class="work-descr">Столовая</div>
                        </div></a></li>
                <li class="work-item kafe"><a href="portfolio_single_featured_image1.html">
                        <div class="work-image"><img src="{{ asset('assets/images/caffe4.jpg') }}" alt="Portfolio Item"/></div>
                        <div class="work-caption font-alt">
                            <h3 class="work-title">Кофетун</h3>
                            <div class="work-descr">Кафе</div>
                        </div></a>
                </li>
                <li class="work-item kuhni"><a href="portfolio_single_featured_image1.html">
                        <div class="work-image"><img src="{{ asset('assets/images/vostok.jpg') }}" alt="Portfolio Item"/></div>
                        <div class="work-caption font-alt">
                            <h3 class="work-title">Восточная кухня</h3>
                            <div class="work-descr">Кухня</div>
                        </div></a>
                </li>
                <li class="work-item stolovie"><a href="portfolio_single_featured_image2.html">
                        <div class="work-image"><img src="{{ asset('assets/images/stolovka4.jpg') }}" alt="Portfolio Item"/></div>
                        <div class="work-caption font-alt">
                            <h3 class="work-title">«У Мадины»</h3>
                            <div class="work-descr">Столовая</div>
                        </div></a></li>
                <li class="work-item kuhni"><a href="portfolio_single_featured_image1.html">
                        <div class="work-image"><img src="{{ asset('assets/images/evropa5.jpg') }}" alt="Portfolio Item"/></div>
                        <div class="work-caption font-alt">
                            <h3 class="work-title">Европейская кухня</h3>
                            <div class="work-descr">Кухня</div>
                        </div></a>
                </li>
                <li class="work-item stolovie"><a href="portfolio_single_featured_image2.html">
                        <div class="work-image"><img src="{{ asset('assets/images/stolovka3.jpg') }}" alt="Portfolio Item"/></div>
                        <div class="work-caption font-alt">
                            <h3 class="work-title">«Чебуречная»</h3>
                            <div class="work-descr">Столовая</div>
                        </div></a></li>
                <li class="work-item kafe"><a href="portfolio_single_featured_image1.html">
                        <div class="work-image"><img src="{{ asset('assets/images/caffe5.jpg') }}" alt="Portfolio Item"/></div>
                        <div class="work-caption font-alt">
                            <h3 class="work-title">Бункер</h3>
                            <div class="work-descr">Кафе</div>
                        </div></a>
                </li>
                <li class="work-item kuhni"><a href="portfolio_single_featured_image1.html">
                        <div class="work-image"><img src="{{ asset('assets/images/chech.jpg') }}" alt="Portfolio Item"/></div>
                        <div class="work-caption font-alt">
                            <h3 class="work-title">Национальная кухня</h3>
                            <div class="work-descr">Кухня</div>
                        </div></a>
                </li>
                <li class="work-item kuhni"><a href="portfolio_single_featured_image1.html">
                        <div class="work-image"><img src="{{ asset('assets/images/vostok2.jpg') }}" alt="Portfolio Item"/></div>
                        <div class="work-caption font-alt">
                            <h3 class="work-title">Караван</h3>
                            <div class="work-descr">Кухня</div>
                        </div></a>
                </li>
                <li class="work-item restoran"><a href="portfolio_single_featured_image1.html">
                        <div class="work-image"><img src="{{ asset('assets/images/evropa4.jpg') }}" alt="Portfolio Item"/></div>
                        <div class="work-caption font-alt">
                            <h3 class="work-title">Ресторан "Paradise"</h3>
                            <div class="work-descr">Ресторан</div>
                        </div></a>
                </li>
                <li class="work-item kafe"><a href="portfolio_single_featured_image1.html">
                        <div class="work-image"><img src="{{ asset('assets/images/caffe6.jpg') }}" alt="Portfolio Item"/></div>
                        <div class="work-caption font-alt">
                            <h3 class="work-title">Восток</h3>
                            <div class="work-descr">Кафе</div>
                        </div></a>
                </li>
                <li class="work-item kuhni"><a href="portfolio_single_featured_image1.html">
                        <div class="work-image"><img src="{{ asset('assets/images/vostok4.jpg') }}" alt="Portfolio Item"/></div>
                        <div class="work-caption font-alt">
                            <h3 class="work-title">Султанская</h3>
                            <div class="work-descr">Кухня</div>
                        </div></a>
                </li>
                <li class="work-item restoran"><a href="portfolio_single_featured_image1.html">
                        <div class="work-image"><img src="{{ asset('assets/images/evropa3.jpg') }}" alt="Portfolio Item"/></div>
                        <div class="work-caption font-alt">
                            <h3 class="work-title">Ресторан "Patio"</h3>
                            <div class="work-descr">Ресторан</div>
                        </div></a>
                </li>
                <li class="work-item restoran"><a href="portfolio_single_featured_image1.html">
                        <div class="work-image"><img src="{{ asset('assets/images/evropa2.jpg') }}" alt="Portfolio Item"/></div>
                        <div class="work-caption font-alt">
                            <h3 class="work-title">Ресторан "Wasabi"</h3>
                            <div class="work-descr">Ресторан</div>
                        </div></a>
                </li>
                <li class="work-item kuhni"><a href="portfolio_single_featured_image1.html">
                        <div class="work-image"><img src="{{ asset('assets/images/vostok3.jpg') }}" alt="Portfolio Item"/></div>
                        <div class="work-caption font-alt">
                            <h3 class="work-title">Шехерезада</h3>
                            <div class="work-descr">Кухня</div>
                        </div></a>
                </li>
                <li class="work-item restoran"><a href="portfolio_single_featured_image1.html">
                        <div class="work-image"><img src="{{ asset('assets/images/evropa.jpg') }}" alt="Portfolio Item"/></div>
                        <div class="work-caption font-alt">
                            <h3 class="work-title">Ресторан "Япона Хата"</h3>
                            <div class="work-descr">Ресторан</div>
                        </div></a>
                </li>
            </ul>
        </div>  <!-- kuhni -->
    </section>
@endsection
