<div class="module-small bg-dark">
    <div id="foot" class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="widget">
                    <div>
                        <img id="logo-foot" src="{{ asset('/assets/images/logo_grozdaar_v.png') }}" alt="">
                    </div>
                    <p>Phone: <a href="tel:+79950959595">+7 (995) 095-95-95</a> </br>
                        Email:<a href="mailto:support@grozdaar.ru"> support@grozdaar.ru</a></br>
                        Адрес: <a href="{{ url('https://yandex.ru/maps/1106/grozniy/house/moskovskaya_ulitsa_33/YEwYcQdkTkcPQFppfX9wcnpqYg==/?ll=45.704728%2C43.313696&utm_source=main_stripe_big&z=16.68') }}">г. Грозный, ул. Маковская 33</a></p>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="widget">
                    <h5 class="widget-title font-alt">Последние комментарии</h5>
                    <ul class="icon-list">
                        <li>Maria on <a href="#">Designer Desk Essentials</a></li>
                        <li>John on <a href="#">Realistic Business Card Mockup</a></li>
                        <li>Andy on <a href="#">Eco bag Mockup</a></li>
                        <li>Jack on <a href="#">Bottle Mockup</a></li>
                        <li>Mark on <a href="#">Our trip to the Alps</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="widget">
                    <h5 class="widget-title font-alt">Blog Categories</h5>
                    <ul class="icon-list">
                        <li><a href="#">Photography - 7</a></li>
                        <li><a href="#">Web Design - 3</a></li>
                        <li><a href="#">Illustration - 12</a></li>
                        <li><a href="#">Marketing - 1</a></li>
                        <li><a href="#">Wordpress - 16</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="widget">
                    <h5 class="widget-title font-alt">Информация</h5>
                    <ul class="icon-list">
                        <li><a href="#">О нас</a></li>
                        <li><a href="#">Вакансии</a></li>
                        <li><a href="#">Рекламодателям</a></li>
                        <li><a href="#">Договор-Оферта</a></li>
                        <li><a href="#">Политика Конфиденциальности</a></li>
                        <li><a href="#">Поддержка</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<hr class="divider-d">
<footer class="footer bg-dark">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <p class="copyright font-alt">&copy; 2022&nbsp;<a href="{{ url('/') }}">GrozDaar</a>. Все права защищены</p>
            </div>
            <div class="col-sm-6">
                <div class="footer-social-links"><a href="{{ url('https://vk.com/grozdaar') }}"><i class="fa fa-vk"></i></a><a href="{{ url('https://t.me/grozdaar') }}"><i class="fa icon-paper-plane-empty"></i></a><a href="{{ url('https://instagram.com/grozdaar') }}"><i class="fa fa-instagram"></i></a>
                </div>
            </div>
        </div>
    </div>
</footer>
