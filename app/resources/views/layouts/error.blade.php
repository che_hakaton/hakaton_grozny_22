<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('layouts.head')
</head>
    <body data-spy="scroll" data-target=".onpage-navigation" data-offset="60">
        <div class="min-h-screen bg-gray-100">
            <!-- Page Content -->
            <main>
                <div class="page-loader">
                    <div class="loader">Загрузка...</div>
                </div>
                <div class="main">
                    {{ $slot }}
                    @include('layouts.footer')
                    <div class="scroll-up"><a href="#totop"><i class="fa fa-angle-double-up"></i></a></div>
                </div>
            </main>
        </div>
        <!-- Scripts -->
        @include('layouts.js')
    </body>
</html>
