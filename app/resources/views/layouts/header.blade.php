<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#custom-collapse"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button><a class="navbar-brand" href="{{ url('/') }}"> <img src="{{ asset('/assets/images/logo_grozdaar.png') }}" alt=""></a>
        </div>
        <div class="collapse navbar-collapse" id="custom-collapse">
            <ul class="nav navbar-nav navbar-right">
                @foreach($topmenus as $topmenu)
                        @if(!$topmenu->children->isEmpty())
                        <li class="dropdown"><a class="dropdown-toggle" href="/" data-toggle="dropdown">{{ $topmenu->name }}</a>
                            <ul class="dropdown-menu">
                                @foreach($topmenu->children as $ChilTopmenu)
                                    @if(!$ChilTopmenu->children->isEmpty())
                                        <li class="dropdown"><a class="dropdown-toggle" href="/" data-toggle="dropdown">{{ $ChilTopmenu->name }}</a>
                                            <ul class="dropdown-menu">
                                                @foreach($ChilTopmenu->children as $ChilTwoTopmenu)
                                                    <li><a href="/">{{ $ChilTwoTopmenu->name }}</a></li>
                                                @endforeach
                                            </ul>
                                        </li>
                                    @else
                                        @if($ChilTopmenu->establishments)
                                            <li class="dropdown"><a class="dropdown-toggle" href="/" data-toggle="dropdown">{{ $ChilTopmenu->name }}</a>
                                                <ul class="dropdown-menu">
                                                    @foreach($ChilTopmenu->establishments as $establishment)
                                                        <li><a href="/">{{ $establishment->name }}</a></li>
                                                    @endforeach
                                                </ul>
                                            </li>
                                        @else
                                        <li><a href="/">{{ $ChilTopmenu->name }}</a></li>
                                        @endif
                                    @endif
                                @endforeach
                            </ul>
                        </li>
                        @else
                            @if($topmenu->establishments)
                                <li class="dropdown"><a class="dropdown-toggle" href="/" data-toggle="dropdown">{{ $topmenu->name }}</a>
                                    <ul class="dropdown-menu">
                                        @foreach($topmenu->establishments as $establishment)
                                            <li><a href="/">{{ $establishment->name }}</a></li>
                                        @endforeach
                                    </ul>
                                </li>
                            @else
                            <li class="dropdown"><a href="/">{{ $topmenu->name }}</a></li>
                            @endif

                        @endif
                @endforeach
                <li class="dropdown"><a href="{{ route('about') }}">О нас</a></li>
                <li class="dropdown"><a href="{{ route('login') }}"><i class="icon-user-circle"></i></a></li>
            </ul>
        </div>
    </div>
</nav>
