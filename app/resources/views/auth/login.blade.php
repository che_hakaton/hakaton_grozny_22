@extends("layouts.app")
@section("content")
    <section class="module bg-dark-30" data-background="assets/images/section-4.jpg">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <h1 class="module-title font-alt mb-0">Вход</h1>
                </div>
            </div>
        </div>
    </section>
    <section class="module">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-sm-offset-4 mb-sm-40">
                    <h4 class="font-alt">Войти</h4>
                    <hr class="divider-w mb-10">
                    <!-- Session Status -->
                    <x-auth-session-status class="mb-4 alert-danger" :status="session('status')" />

                    <!-- Validation Errors -->
                    <x-auth-validation-errors class="mb-4 alert-danger" :errors="$errors" />
                    <form class="form" method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group">
                            <x-input id="email" class="form-control" type="email" name="email" :value="old('email')" placeholder="Email" required autofocus />
                        </div>
                        <div class="form-group">
                            <x-input id="password" class="form-control"
                                     type="password"
                                     name="password"
                                     required autocomplete="current-password"
                                     placeholder="Пароль" />
                        </div>
                        <div class="form-group">
                            <button class="btn btn-round btn-b">Войти</button>
                        </div>
                        <div class="form-group">
                            <a href="{{ route('password.request') }}">Забыли пароль?</a></br>
                            <a href="{{ route('register') }}">Если нет аккаунта, создать.</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
