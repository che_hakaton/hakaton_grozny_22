@extends("layouts.app")
@section("content")
    <section class="module bg-dark-30" data-background="assets/images/section-4.jpg">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <h1 class="module-title font-alt mb-0">Регистрация</h1>
                </div>
            </div>
        </div>
    </section>
    <section class="module">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-lg-offset-4">
                    <h4 class="font-alt">Регистрация</h4>
                    <hr class="divider-w mb-10">
                    <!-- Validation Errors -->
                    <x-auth-validation-errors class="mb-4 alert-danger" :errors="$errors" />
                    <form class="form" method="POST" action="{{ route('register') }}">
                        @csrf
                        <div class="form-group">
                            <x-input id="email" class="form-control" type="email" name="email" :value="old('email')" placeholder="Email" required />
                        </div>
                        <div class="form-group">
                            <x-input id="name" class="form-control" type="text" name="name" :value="old('name')" placeholder="Ваше имя" required autofocus />
                        </div>
                        <div class="form-group">
                            <x-input id="password" class="form-control"
                                     type="password"
                                     name="password"
                                     required autocomplete="new-password"
                                     placeholder="Пароль" />
                        </div>
                        <div class="form-group">
                            <x-input id="password_confirmation" class="form-control"
                                     type="password"
                                     name="password_confirmation"
                                     placeholder="Повторите пароль" required />
                        </div>
                        <div class="form-group">
                            <button class="btn btn-block btn-round btn-b">Регистрация</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
