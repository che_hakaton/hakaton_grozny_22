<x-error-layout>
    <section class="home-section home-parallax home-fade home-full-height bg-dark bg-dark-30" id="home" data-background="{{ asset('assets/images/section-4.jpg') }}">
        <div class="titan-caption">
            <div class="caption-content">

                @if (session('status') == 'verification-link-sent')
                    <div class="font-alt">
                        На адрес электронной почты, который вы указали при регистрации, была отправлена новая ссылка для подтверждения.
                    </div>
                @endif

                <div class="mt-4 flex items-center justify-between">
                    <form method="POST" action="{{ route('verification.send') }}">
                        @csrf

                        <div class="font-alt mt-30">
                            <x-button class="btn btn-border-w btn-round">
                                Выслать повторно письмо для подтверждения
                            </x-button>
                        </div>
                    </form>

                    <form method="POST" action="{{ route('logout') }}">
                        @csrf
                        <div class="font-alt mt-30">
                            <button type="submit" class="btn btn-border-w btn-round">
                                {{ __('Выйти') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</x-error-layout>
