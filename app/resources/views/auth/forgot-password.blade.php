@extends("layouts.app")
@section("content")
    <section class="module bg-dark-30" data-background="assets/images/section-4.jpg">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <h1 class="module-title font-alt mb-0">Восстановление доступа</h1>
                </div>
            </div>
        </div>
    </section>
    <section class="module">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-sm-offset-4 mb-sm-40">
                    <h4 class="font-alt">Сбросить пароль</h4>
                    <hr class="divider-w mb-10">
                    <!-- Session Status -->
                    <x-auth-session-status class="mb-4" :status="session('status')" />

                    <!-- Validation Errors -->
                    <x-auth-validation-errors class="mb-4" :errors="$errors" />
                    <form class="form" method="POST" action="{{ route('password.email') }}">
                        @csrf
                        <div class="form-group">
                            <x-input id="email" class="form-control" type="email" name="email" :value="old('email')" placeholder="Email" required autofocus />
                        </div>
                        <div class="form-group">
                            <button class="btn btn-round btn-b">Сбросить</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
