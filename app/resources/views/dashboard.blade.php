@extends("layouts.app")
@section("content")
    <section class="module bg-dark-30" data-background="{{ asset('assets/images/section-4.jpg') }}">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <h1 class="module-title font-alt mb-0">Вы успешно создали аккаунт!</h1>
                </div>
            </div>
        </div>
    </section>
@endsection
