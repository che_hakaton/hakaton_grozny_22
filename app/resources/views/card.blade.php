@extends("layouts.app")
@section("content")
    <section class="module">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-md-6 col-lg-6"><img src="assets/images/adelishka_restoran.jpg" alt="Ресторан Аделишка"/></div>
                <div class="col-sm-8 col-md-6 col-lg-6">
                    <div class="work-details">
                        <h5 class="work-details-title font-alt">Ресторан "Аделишка"</h5>
                        <div class="row mb-20">
                            <div class="col-sm-12"><span><i class="fa fa-star star" size="lg"></i></span><span><i class="fa fa-star star"></i></span><span><i class="fa fa-star star"></i></span><span><i class="fa fa-star star"></i></span><span><i class="fa fa-star star-off"></i></span><a class="open-tab section-scroll" href="#reviews">- 2 проголосовало</a>
                            </div>
                        </div>
                        <p>В данном параграфе будеи представлено описание, как правило занимающее от 15 до 30 слов. Слишком длинные описания менее распространены.</p>
                        <ul>
                            <li><strong>Компания: </strong><span class="font-serif"><a href="#" target="_blank">ИП Мусагалиева Разет Баймирзовна</a></span>
                            </li>
                            <li><strong>Дата регистрации: </strong><span class="font-serif"><a href="#" target="_blank">23 Ноября, 2022</a></span>
                            </li>
                            <li><strong>Сайт: </strong><span class="font-serif"><a href="#" target="_blank">www.site.com</a></span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
    </section>
    <section>
        <div class="row tagsComment">
            <div class="col-sm-8 col-sm-offset-2">
                <div class="tags font-serif">
                    <a href="#" rel="tag">Восточная Кухня</a>
                    <a href="#" rel="tag">Кафе</a>
                    <a href="#" rel="tag">Завтрак</a>
                    <a href="#" rel="tag">Обед</a>
                    <a href="#" rel="tag">Ужин</a>
                    <a href="#" rel="tag">Манты</a>
                    <a href="#" rel="tag">Стейк</a>
                    <a href="#" rel="tag">Шашлык</a>
                    <a href="#" rel="tag">Плов</a>
                    <a href="#" rel="tag">Хачапури</a>
                    <a href="#" rel="tag">Хинкали</a>
                    <a href="#" rel="tag">Блины</a>
                    <a href="#" rel="tag">Самса</a>
                    <a href="#" rel="tag">Рыба</a>
                    <a href="#" rel="tag">Цезарь</a>
                    <a href="#" rel="tag">Мясо по Французски</a>
                    <a href="#" rel="tag">Хаш</a>
                    <a href="#" rel="tag">Самса</a>
                    <a href="#" rel="tag">Тандыр-Самса</a>
                    <a href="#" rel="tag">Курица на углях</a>
                    <a href="#" rel="tag">Чепубели</a>
                    <a href="#" rel="tag">Форель</a>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="card">
            <div class="col-sm-8 col-sm-offset-2">
                <div class="comment-box ml-2">
                    <div class="rating">
                        <input type="radio" name="rating" value="5" id="5"><label for="5">☆</label>
                        <input type="radio" name="rating" value="4" id="4"><label for="4">☆</label>
                        <input type="radio" name="rating" value="3" id="3"><label for="3">☆</label>
                        <input type="radio" name="rating" value="2" id="2"><label for="2">☆</label>
                        <input type="radio" name="rating" value="1" id="1"><label for="1">☆</label>
                    </div>
                    <div class="comment-area">
                        <textarea class="form-control" placeholder="О чем хотите сообщить?" rows="4"></textarea>
                    </div>
                    <div class="comment-btns mt-2">
                        <div class="row">
                            <div class="col-6">
                                <div class="pull-left">
                                    <button id="btn" class="btn btn-pro btn-sm">Отменить</button>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="pull-right">
                                    <button id="btn" class="btn btn-pro send btn-sm">Отправить <i class="fa fa-long-arrow-right ml-1"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
    <section>
        <div id="commentList" class="container">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2">
                    <div class="wrapper_reiting">
                        <div class="row mb-20" id="reiting">
                            <div class="col-sm-12"><span><i class="fa fa-star star" size="lg"></i></span><span><i class="fa fa-star star"></i></span><span><i class="fa fa-star star"></i></span><span><i class="fa fa-star star"></i></span><span><i class="fa fa-star star-off"></i></span></div>
                        </div>
                        <ul>
                            <li class="date">22:28 29.12.2022</li>
                            <li class="name">Ярослава</li>
                        </ul>
                        <p>Отличный ресторан, ездили всей семьей на протяжении всего отпуска на обеды и ужины</p>
                    </div>
                    <div class="wrapper_reiting">
                        <div class="row mb-20" id="reiting">
                            <div class="col-sm-12"><span><i class="fa fa-star star" size="lg"></i></span><span><i class="fa fa-star star"></i></span><span><i class="fa fa-star star"></i></span><span><i class="fa fa-star star"></i></span><span><i class="fa fa-star star-off"></i></span></div>
                        </div>
                        <ul>
                            <li class="date">22:28 29.12.2022</li>
                            <li class="name">Никита</li>
                        </ul>
                        <p>Отличный ресторан, ездили всей семьей на протяжении всего отпуска на обеды и ужины</p>
                    </div>
                    <div class="wrapper_reiting">
                        <div class="row mb-20" id="reiting">
                            <div class="col-sm-12"><span><i class="fa fa-star star" size="lg"></i></span><span><i class="fa fa-star star"></i></span><span><i class="fa fa-star star"></i></span><span><i class="fa fa-star star"></i></span><span><i class="fa fa-star star-off"></i></span></div>
                        </div>
                        <ul>
                            <li class="date">22:28 29.12.2022</li>
                            <li class="name">Наталья</li>
                        </ul>
                        <p>Отличный ресторан, ездили всей семьей на протяжении всего отпуска на обеды и ужины</p>
                    </div>
                    <div class="wrapper_reiting">
                        <div class="row mb-20" id="reiting">
                            <div class="col-sm-12"><span><i class="fa fa-star star" size="lg"></i></span><span><i class="fa fa-star star"></i></span><span><i class="fa fa-star star"></i></span><span><i class="fa fa-star star"></i></span><span><i class="fa fa-star star-off"></i></span></div>
                        </div>
                        <ul>
                            <li class="date">22:28 29.12.2022</li>
                            <li class="name">Сергей</li>
                        </ul>
                        <p>Отличный ресторан, ездили всей семьей на протяжении всего отпуска на обеды и ужины</p>
                    </div>
                    <div class="wrapper_reiting">
                        <div class="row mb-20" id="reiting">
                            <div class="col-sm-12"><span><i class="fa fa-star star" size="lg"></i></span><span><i class="fa fa-star star"></i></span><span><i class="fa fa-star star"></i></span><span><i class="fa fa-star star"></i></span><span><i class="fa fa-star star-off"></i></span></div>
                        </div>
                        <ul>
                            <li class="date">22:28 29.12.2022</li>
                            <li class="name">Индира</li>
                        </ul>
                        <p>Отличный ресторан, ездили всей семьей на протяжении всего отпуска на обеды и ужины</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
