#!/bin/sh

set -e

crond

supervisord --configuration /etc/supervisord.conf

exec docker-php-entrypoint "$@"